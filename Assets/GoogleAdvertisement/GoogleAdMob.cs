﻿using System;
using System.Collections.Generic;
using GoogleMobileAds.Api;
using GoogleMobileAds.Api.Mediation;
using UnityEngine;

namespace GoogleAdvertisement
{
    /// <inheritdoc />
    /// <summary>
    ///     Google ad mob.
    /// </summary>
    public class GoogleAdMob : MonoBehaviour
    {
        #region Public

        /// <summary>
        ///     Google Ad mobManager.
        /// </summary>
        public static GoogleAdMob GoogleAdMobReference;

        /// <summary>
        ///     Gender.
        /// </summary>
        public enum Gender
        {
            Male,
            Female,
            Undefined
        }

        /// <summary>
        ///     Products.
        /// </summary>
        public enum Products
        {
            Banner,
            Interstitial,
            RewardBasedVideo
        }

        /// <summary>
        ///     Products.
        /// </summary>
        public List<Products> ListOfProducts;

        /// <summary>
        ///     The keywords for advertisement.
        /// </summary>
        [Header("Advertisement KeyWords")] [Space(10f)]
        public List<string> KeywordsForAdvertisement;

        /// <summary>
        ///     The advertisement for particular gender.
        /// </summary>
        [Header("Advertisement Target Gender")] [Space(10f)]
        public Gender AdvertisementForParticularGender;

        /// <summary>
        ///     The child directed treatment tag.
        /// </summary>
        [Header("ChildDirectedTreatmentTag")] [Space(10f)]
        public bool ChildDirectedTreatmentTag;

        /// <summary>
        ///     The birth date for ad viewer.
        /// </summary>
        public DateTime? BirthDateForAdViewer;

        /// <summary>
        ///     The add extra dictionary.
        /// </summary>
        public Dictionary<string, string> AddExtraDictionary;

        /// <summary>
        ///     The mediation.
        ///     Other mediation for google ad mob .
        /// </summary>
        public MediationExtras Mediation;

        #endregion

        #region Private Variables

        /// <summary>
        ///     App Id .
        /// </summary>
        private static string _appId;

        /// <summary>
        ///     Banner Id .
        /// </summary>
        private static string _bannerId;

        /// <summary>
        ///     Interstitial Id .
        /// </summary>
        private static string _interstitialId;

        /// <summary>
        ///     Reward Video Id .
        /// </summary>
        private static string _rewardVideoId;

        /// <summary>
        ///     The banner view.
        /// </summary>
        private BannerView _bannerView;

        /// <summary>
        ///     The interstitial.
        /// </summary>
        private InterstitialAd _interstitial;

        /// <summary>
        ///     The reward based video.
        /// </summary>
        private static RewardBasedVideoAd _rewardBasedVideo;

        #endregion

        /// <summary>
        ///     Awake the instance .
        /// </summary>
        private void Awake()
        {
            if (GoogleAdMobReference == null) GoogleAdMobReference = this;

            //Init The instance .
            Init();
        }

        /// <summary>
        ///     Init the test adds.
        /// </summary>
        private void InitTestAdds()
        {
            //Declaration
            switch (GoogleAdMobController.GoogleAdMobControllerReference.ShowTestAdd)
            {
                //When using Google test app id .
                case GoogleAdMobController.TestAdType.UsingGoogleTestAppId:
                    //Debug.
                    Debug.Log("Google Free Test App id will be used .");
                    //Now check the os type .
                    switch (GoogleAdMobController.GoogleAdMobControllerReference.TargetDeviceType)
                    {
                        case GoogleAdMobController.DeviceType.Android:
                            _appId = GoogleAdMobController.GoogleAdMobControllerReference.AndroidAppId;
                            _bannerId = GoogleAdMobTestIdController.ANDROID_TEST_BANNER_APP_ID;
                            _interstitialId = GoogleAdMobTestIdController.ANDROID_TEST_INTERSTITIAL_APP_ID;
                            _rewardVideoId = GoogleAdMobTestIdController.ANDROID_TEST_REWARD_VIDEO_APP_ID;
                            break;
                        case GoogleAdMobController.DeviceType.Ios:
                            _appId = GoogleAdMobController.GoogleAdMobControllerReference.IosAppId;
                            _bannerId = GoogleAdMobTestIdController.ANDROID_TEST_BANNER_APP_ID;
                            _interstitialId = GoogleAdMobTestIdController.ANDROID_TEST_INTERSTITIAL_APP_ID;
                            _rewardVideoId = GoogleAdMobTestIdController.ANDROID_TEST_REWARD_VIDEO_APP_ID;
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }

                    break;
                //When using Device identifier .
                case GoogleAdMobController.TestAdType.UsingTestDeviceIdentifier:
                    Debug.Log("Test Device identifier will be used .");
                    //Check Test Device identifiers.
                    Debug.Log("Please check test device identifiers .");
                    //Now check the os type .
                    switch (GoogleAdMobController.GoogleAdMobControllerReference.TargetDeviceType)
                    {
                        case GoogleAdMobController.DeviceType.Android:
                            _appId = GoogleAdMobController.GoogleAdMobControllerReference.AndroidAppId;
                            _bannerId = GoogleAdMobController.GoogleAdMobControllerReference.AndroidBannerAppId;
                            _interstitialId = GoogleAdMobController.GoogleAdMobControllerReference
                                .AndroidInterstitialAppId;
                            _rewardVideoId = GoogleAdMobController.GoogleAdMobControllerReference
                                .AndroidRewardVideoAppId;
                            break;
                        case GoogleAdMobController.DeviceType.Ios:
                            _appId = GoogleAdMobController.GoogleAdMobControllerReference.IosAppId;
                            _bannerId = GoogleAdMobController.GoogleAdMobControllerReference.IosBannerAppId;
                            _interstitialId = GoogleAdMobController.GoogleAdMobControllerReference.IosInterstitialAppId;
                            _rewardVideoId = GoogleAdMobController.GoogleAdMobControllerReference
                                .IosRewardVideoAppId;
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }

                    //If no test device identifier is Added .
                    if (GoogleAdMobController.GoogleAdMobControllerReference.TestDevices.Count <= 0)
                    {
                        Debug.LogWarning("Please Add Test Device identifiers for showing test Adds ");
                        GoogleAdMobController.GoogleAdMobControllerReference.ShowTestAdd =
                            GoogleAdMobController.TestAdType.UsingGoogleTestAppId;
                    }

                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            //Init Test Add.
            // Initialize the Google Mobile Ads SDK.
            MobileAds.Initialize(_appId);
            //Init Reward Based video.
            InitRewardBasedVideo();
        }

        /// <summary>
        ///     Init Distribution Ads .
        /// </summary>
        private void InitDistributionAds()
        {
            switch (GoogleAdMobController.GoogleAdMobControllerReference.TargetDeviceType)
            {
                case GoogleAdMobController.DeviceType.Android:
                    //Set up ids.
                    _appId = GoogleAdMobController.GoogleAdMobControllerReference.AndroidAppId;
                    _bannerId = GoogleAdMobController.GoogleAdMobControllerReference.AndroidBannerAppId;
                    _interstitialId = GoogleAdMobController.GoogleAdMobControllerReference.AndroidInterstitialAppId;
                    _rewardVideoId = GoogleAdMobController.GoogleAdMobControllerReference
                        .AndroidRewardVideoAppId;

                    //if AndroidAppId empty or null.
                    if (string.IsNullOrEmpty(_appId))
                    {
                        Debug.LogWarning("AndroidAppId is not set up .");
                    }
                    else
                    {
                        // Initialize the Google Mobile Ads SDK.
                        MobileAds.Initialize(_appId);
                        //Init Reward Based video.
                        InitRewardBasedVideo();
                    }

                    break;

                case GoogleAdMobController.DeviceType.Ios:
                    //Set up plat form.
                    if (Application.platform == RuntimePlatform.IPhonePlayer)
                        MobileAds.SetiOSAppPauseOnBackground(true);
                    //Set up ids.
                    _appId = GoogleAdMobController.GoogleAdMobControllerReference.IosAppId;
                    _bannerId = GoogleAdMobController.GoogleAdMobControllerReference.IosBannerAppId;
                    _interstitialId = GoogleAdMobController.GoogleAdMobControllerReference.IosInterstitialAppId;
                    _rewardVideoId = GoogleAdMobController.GoogleAdMobControllerReference.IosRewardVideoAppId;
                    //if IosAppId empty or null.
                    if (!string.IsNullOrEmpty(_appId))
                    {
                        Debug.LogWarning("IosAppId is not set up .");
                    }
                    else
                    {
                        // Initialize the Google Mobile Ads SDK.
                        MobileAds.Initialize(_appId);
                        //Init Reward Based video.
                        InitRewardBasedVideo();
                    }

                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        ///     Init this instance.
        /// </summary>
        private void Init()
        {
            //First check development or distribution .
            switch (GoogleAdMobController.GoogleAdMobControllerReference.CurrentGoogleAdMobManagerType)
            {
                case GoogleAdMobController.GoogleAdMobManagerType.Development:
                    //If development init test adds.
                    InitTestAdds();
                    //Make all request.
                    MakeAllRequest();
                    return;
                case GoogleAdMobController.GoogleAdMobManagerType.Distribution:
                    //If Distribution init real adds.
                    InitDistributionAds();
                    //Make all request.
                    MakeAllRequest();
                    return;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        ///     Init the reward based video.
        ///     Reward Based Video will be instanced only once .
        /// </summary>
        private void InitRewardBasedVideo()
        {
            // Get singleton reward based video ad reference.
            _rewardBasedVideo = RewardBasedVideoAd.Instance;
            // RewardBasedVideoAd is a singleton, so handlers should only be registered once.
            _rewardBasedVideo.OnAdLoaded += HandleRewardBasedVideoLoaded;
            _rewardBasedVideo.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
            _rewardBasedVideo.OnAdOpening += HandleRewardBasedVideoOpened;
            _rewardBasedVideo.OnAdStarted += HandleRewardBasedVideoStarted;
            _rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
            _rewardBasedVideo.OnAdClosed += HandleRewardBasedVideoClosed;
            _rewardBasedVideo.OnAdLeavingApplication += HandleRewardBasedVideoLeftApplication;
        }


        /// <summary>
        ///     Creates the ad request.
        /// </summary>
        /// <returns>The ad request.</returns>
        private AdRequest CreateAdRequest()
        {
            //First check the development or distribution .
            switch (GoogleAdMobController.GoogleAdMobControllerReference.CurrentGoogleAdMobManagerType)
            {
                //If development .
                case GoogleAdMobController.GoogleAdMobManagerType.Development:
                    //Create the request .
                    var builder = new AdRequest.Builder();
                    //Add test devices.
                    foreach (var item in GoogleAdMobController.GoogleAdMobControllerReference.TestDevices)
                        builder.AddTestDevice(item);

                    //Add keywords.
                    foreach (var item in KeywordsForAdvertisement) builder.AddTestDevice(item);

                    //Add Birth Date.
                    if (BirthDateForAdViewer != null) builder.SetBirthday(BirthDateForAdViewer.Value);

                    //Set gender.
                    switch (AdvertisementForParticularGender)
                    {
                        case Gender.Male:
                            builder.SetGender(GoogleMobileAds.Api.Gender.Male);
                            break;
                        case Gender.Female:
                            builder.SetGender(GoogleMobileAds.Api.Gender.Female);
                            break;
                        case Gender.Undefined:
                            builder.SetGender(GoogleMobileAds.Api.Gender.Unknown);
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }

                    //Add mediation extras.
                    if (Mediation != null) builder.AddMediationExtras(Mediation);
                    //Set TagForChildDirectedTreatment
                    builder.TagForChildDirectedTreatment(ChildDirectedTreatmentTag);
                    //Check Extra dictionary is not null .
                    if (AddExtraDictionary == null) return builder.Build();
                    //Set extra.
                    foreach (var item in AddExtraDictionary) builder.AddExtra(item.Key, item.Value);
                    return builder.Build();
                //If distribution .
                case GoogleAdMobController.GoogleAdMobManagerType.Distribution:
                    //Create the request .
                    return new AdRequest.Builder().Build();
                //Give error .
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }


        #region Banner callback handlers

        public void HandleAdLoaded(object sender, EventArgs args)
        {
            Debug.Log("HandleAdLoaded event received For Banner.");
        }

        public void HandleAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
        {
            Debug.Log("HandleFailedToReceiveAd event received For Banner with message: " + args.Message);
        }

        public void HandleAdOpened(object sender, EventArgs args)
        {
            Debug.Log("HandleAdOpened  Banner event received");
        }

        public void HandleAdClosed(object sender, EventArgs args)
        {
            Debug.Log("HandleAdClosed Banner event received");
        }

        public void HandleAdLeftApplication(object sender, EventArgs args)
        {
            Debug.Log("HandleAdLeftApplication Banner event received");
        }

        #endregion


        #region Interstitial callback handlers

        public void HandleInterstitialLoaded(object sender, EventArgs args)
        {
            Debug.Log("HandleInterstitialLoaded Interstitial event received");
        }

        public void HandleInterstitialFailedToLoad(object sender, AdFailedToLoadEventArgs args)
        {
            Debug.Log("HandleInterstitialFailedToLoad Interstitial event received with message: " + args.Message);
            //If interstitial is failed to load Just request for another interstitial.
            RequestInterstitial();
        }

        public void HandleInterstitialOpened(object sender, EventArgs args)
        {
            Debug.Log("HandleInterstitialOpened Interstitial event received");
        }

        public void HandleInterstitialClosed(object sender, EventArgs args)
        {
            Debug.Log("HandleInterstitialClosed Interstitial event received");
            //If interstitial is closed Just request for another interstitial for future use.
            RequestInterstitial();
        }

        public void HandleInterstitialLeftApplication(object sender, EventArgs args)
        {
            Debug.Log("HandleInterstitialLeftApplication Interstitial event received");
        }

        #endregion


        #region RewardBasedVideo callback handlers

        public void HandleRewardBasedVideoLoaded(object sender, EventArgs args)
        {
            Debug.Log("HandleRewardBasedVideoLoaded event received");
        }

        public void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args)
        {
            Debug.Log("HandleRewardBasedVideoFailedToLoad RewardBasedVideo event received with message: " + args.Message);
            //If reward based video if failed to load request again.
            RequestRewardBasedVideo();
        }

        public void HandleRewardBasedVideoOpened(object sender, EventArgs args)
        {
            Debug.Log("HandleRewardBasedVideoOpened RewardBasedVideo event received");
        }

        public void HandleRewardBasedVideoStarted(object sender, EventArgs args)
        {
            Debug.Log("HandleRewardBasedVideoStarted RewardBasedVideo event received");
        }

        public void HandleRewardBasedVideoClosed(object sender, EventArgs args)
        {
            Debug.Log("HandleRewardBasedVideoClosed RewardBasedVideo event received");
            //If reward based video is closed  load request again for future .
            RequestRewardBasedVideo();
        }

        public void HandleRewardBasedVideoRewarded(object sender, Reward args)
        {
            Debug.Log("We should give him some reward----->"+sender);
            if (args == null) return;
            var type = args.Type;
            var amount = args.Amount;
            Debug.Log("HandleRewardBasedVideoRewarded event received for " + amount + " " + type);
        }

        public void HandleRewardBasedVideoLeftApplication(object sender, EventArgs args)
        {
            Debug.Log("HandleRewardBasedVideoLeftApplication event received");
        }

        #endregion


        #region Request Region

        /// <summary>
        ///     Make all request .
        /// </summary>
        private void MakeAllRequest()
        {
            if (ListOfProducts.Contains(Products.Banner)) RequestBanner();

            if (ListOfProducts.Contains(Products.Interstitial)) RequestInterstitial();

            if (ListOfProducts.Contains(Products.RewardBasedVideo)) RequestRewardBasedVideo();
        }

        /// <summary>
        ///     Requests the banner.
        /// </summary>
        private void RequestBanner()
        {
            //If no banner id is assigned.
            if (string.IsNullOrEmpty(_bannerId)) return;

            // Clean up banner ad before creating a new one.
            if (_bannerView != null) _bannerView.Destroy();

            // Create a 320x50 banner at the top of the screen.
            _bannerView = new BannerView(_bannerId, AdSize.SmartBanner, AdPosition.Top);

            // Register for ad events.
            _bannerView.OnAdLoaded += HandleAdLoaded;
            _bannerView.OnAdFailedToLoad += HandleAdFailedToLoad;
            _bannerView.OnAdOpening += HandleAdOpened;
            _bannerView.OnAdClosed += HandleAdClosed;
            _bannerView.OnAdLeavingApplication += HandleAdLeftApplication;

            // Load a banner ad.
            _bannerView.LoadAd(CreateAdRequest());
        }

        /// <summary>
        ///     Requests the interstitial.
        /// </summary>
        private void RequestInterstitial()
        {
            //If No Interstitial is assigned.
            if (string.IsNullOrEmpty(_interstitialId)) return;

            // Clean up interstitial ad before creating a new one.
            if (_interstitial != null) _interstitial.Destroy();

            // Create an interstitial.
            _interstitial = new InterstitialAd(_interstitialId);

            // Register for ad events.
            _interstitial.OnAdLoaded += HandleInterstitialLoaded;
            _interstitial.OnAdFailedToLoad += HandleInterstitialFailedToLoad;
            _interstitial.OnAdOpening += HandleInterstitialOpened;
            _interstitial.OnAdClosed += HandleInterstitialClosed;
            _interstitial.OnAdLeavingApplication += HandleInterstitialLeftApplication;

            // Load an interstitial ad.
            _interstitial.LoadAd(CreateAdRequest());
        }

        /// <summary>
        ///     Requests the reward based video.
        /// </summary>
        private void RequestRewardBasedVideo()
        {
            //If No Reward video is assigned.
            if (string.IsNullOrEmpty(_rewardVideoId)) return;

            // Create an RewardBasedVideo.
            _rewardBasedVideo.LoadAd(CreateAdRequest(), _rewardVideoId);
        }

        #endregion


        #region ShowRegion

        /// <summary>
        ///     Show interstitial.
        /// </summary>
        public void ShowInterstitial()
        {
            //First Check the Network Reach ability .
            if (Application.internetReachability == NetworkReachability.NotReachable) return;
            //If interstitial is loaded .
            if (_interstitial.IsLoaded())
                _interstitial.Show();
            else
                Debug.Log("Interstitial is not ready yet");
        }

        /// <summary>
        ///     Show reward based video.
        /// </summary>
        public void ShowRewardBasedVideo()
        {
            //First Check the Network Reach ability .
            if (Application.internetReachability == NetworkReachability.NotReachable) return;
            //Check reward video is able to load or not .
            if (_rewardBasedVideo.IsLoaded())
                _rewardBasedVideo.Show();
            else
                Debug.Log("Reward based video ad is not ready yet");
        }

        /// <summary>
        ///     Show banner.
        /// </summary>
        public void ShowBanner()
        {
            //First Check the Network Reach ability .
            if (Application.internetReachability == NetworkReachability.NotReachable) return;
            //If banner view is null.
            if (_bannerView != null)
            {
                _bannerView.Show();
            }
            else
            {
                //Request the banner.
                RequestBanner();
                //Show banner.
                _bannerView.Show();
            }
        }

        /// <summary>
        ///     Destroy banner.
        /// </summary>
        public void DestroyBanner()
        {
            //Destroy banner when banner is present.
            if (_bannerView != null) _bannerView.Destroy();
        }

        #endregion
    }
}