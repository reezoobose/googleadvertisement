﻿using System.Collections.Generic;
using UnityEngine;

namespace GoogleAdvertisement
{
    /// <inheritdoc />
    /// <summary>
    /// Google ad mob manager.
    /// </summary>
    public class GoogleAdMobController : MonoBehaviour
    {
        /// <summary>
        /// GoogleAdMobController Reference .
        /// </summary>
        public static GoogleAdMobController GoogleAdMobControllerReference;
        /// <summary>
        /// Google ad mob manager type.
        /// </summary>
        public enum GoogleAdMobManagerType
        {
            /// <summary>
            /// For development.
            /// </summary>
            Development,

            /// <summary>
            /// For distribution.
            /// </summary>
            Distribution
        }

        /// <summary>
        /// Device Type .
        /// </summary>
        public enum DeviceType
        {
            /// <summary>
            /// android .
            /// </summary>
            Android,

            /// <summary>
            /// iOS.
            /// </summary>
            Ios,
        }

        /// <summary>
        /// Test ad type.
        /// </summary>
        public enum TestAdType
        {
            UsingGoogleTestAppId,
            UsingTestDeviceIdentifier
        }

        /// <summary>
        /// Show test add.
        /// </summary>
        [Header("Test App Type")]
        [Tooltip("Test add can be shown using test Id Used by Google or Adding test Device Identifier from the log ")]
        public TestAdType ShowTestAdd;

        /// <summary>
        /// The type of the current google ad mob manager.
        /// </summary>
        [Header("Current GoogleAd MobManager Type ")] [Space(10f)]
        public GoogleAdMobManagerType CurrentGoogleAdMobManagerType;

        /// <summary>
        /// Target Device Type .
        /// </summary>
        [Header("Target Device type")] [Space(10f)]
        public DeviceType TargetDeviceType;

        /// <summary>
        /// The android app identifier.
        /// </summary>
        [Header("Android App id")] [Space(10f)]
        public string AndroidAppId;

        /// <summary>
        /// The iOS app identifier.
        /// </summary>
        [Header("ios App id")] [Space(10f)] public string IosAppId;

        /// <summary>
        /// The android reward video app identifier.
        /// </summary>
        [Header("Android Reward video App Id")] [Space(10f)]
        public string AndroidRewardVideoAppId;

        /// <summary>
        /// The iOS Reward video app identifier.
        /// </summary>
        [Header("iOs Reward video App Id")] [Space(20f)]
        public string IosRewardVideoAppId;

        /// <summary>
        /// The android banner app identifier.
        /// </summary>
        [Header("Android Banner App Id")] [Space(10f)]
        public string AndroidBannerAppId;

        /// <summary>
        /// The iOS banner app identifier.
        /// </summary>
        [Header("iOs Banner  App Id")] [Space(20f)]
        public string IosBannerAppId;

        /// <summary>
        /// The android Interstitial app identifier.
        /// </summary>
        [Header("Android interstitial App Id")] [Space(10f)]
        public string AndroidInterstitialAppId;

        /// <summary>
        /// The iOS Interstitial app identifier.
        /// </summary>
        [Header("iOs interstitial  App Id")] [Space(20f)]
        public string IosInterstitialAppId;

        /// <summary>
        /// The test devices.
        /// </summary>
        [Header("Add Test Device.")] [Space(10f)] [Tooltip("Test devices will only get test ad advertisement.")]
        public List<string> TestDevices;

        /// <summary>
        /// Awake the instance .
        /// </summary>
        private void Awake()
        {
            //Check reference is null or not .
            if (GoogleAdMobControllerReference != null) return;
            //Set up the reference .
            GoogleAdMobControllerReference = this;
        }
    }

}