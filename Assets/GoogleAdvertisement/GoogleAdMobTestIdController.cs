﻿namespace GoogleAdvertisement
{
    /// <summary>
    /// Google ad mob test identifier controller.
    /// </summary>
    public sealed class GoogleAdMobTestIdController
    {

        /*
        * The quickest way to enable testing is to use Google-provided test ad units. 
        * These ad units are not associated with your AdMob account, 
        * so there's no risk of your account generating invalid traffic when using these ad units. 
        * Here are sample ad units that point to specific test creatives for each format:       
        */
        /// <summary>
        /// Test Banner App Id .
        /// </summary>
        public static readonly string ANDROID_TEST_BANNER_APP_ID = "ca-app-pub-3940256099942544/6300978111";
        /// <summary>
        /// Test Interstitial AppId.
        /// </summary>
        public static readonly string ANDROID_TEST_INTERSTITIAL_APP_ID = "ca-app-pub-3940256099942544/1033173712";
        /// <summary>
        /// Test Reward Video AppId.
        /// </summary>
        public static readonly string ANDROID_TEST_REWARD_VIDEO_APP_ID = "ca-app-pub-3940256099942544/5224354917";



        /// <summary>
        /// Test Banner App Id .
        /// </summary>
        public static readonly string IOS_TEST_BANNER_APP_ID = "ca-app-pub-3940256099942544/2934735716";
        /// <summary>
        /// Test Interstitial AppId.
        /// </summary>
        public static readonly string IOS_TEST_INTERSTITIAL_APP_ID = "ca-app-pub-3940256099942544/4411468910";
        /// <summary>
        /// Test Reward Video AppId.
        /// </summary>
        public static readonly string IOS_TEST_REWARD_VIDEO_APP_ID = "ca-app-pub-3940256099942544/1712485313";
    }
}