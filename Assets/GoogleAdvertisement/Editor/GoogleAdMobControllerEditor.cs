﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace GoogleAdvertisement.Editor {

    /// <inheritdoc />
    /// <summary>
    /// Google ad mob manager editor.
    /// </summary>
    [CustomEditor(typeof(GoogleAdMobController))]
    public class GoogleAdMobControllerEditor : UnityEditor.Editor {

        /// <inheritdoc/>
        /// <summary>
        /// On inspector Gui.
        /// </summary>
        public override void OnInspectorGUI()
        {
            //Get the Google ad mob manager reference .
            var googleAdMobManager = target as GoogleAdMobController;
            //serialize.
            serializedObject.Update();
            //check null.
            if (googleAdMobManager == null) return;
            //Give space .
            EditorGUILayout.Space();

            //Give a label
            GUILayout.Label("Google AdMobManager Type", EditorStyles.boldLabel);
            //Manager enum.
            googleAdMobManager.CurrentGoogleAdMobManagerType = (GoogleAdMobController.GoogleAdMobManagerType) EditorGUILayout.EnumPopup(
                "GoogleAdMobController Type",
                googleAdMobManager.CurrentGoogleAdMobManagerType);


            //If development .
            if (googleAdMobManager.CurrentGoogleAdMobManagerType ==
                GoogleAdMobController.GoogleAdMobManagerType.Development)
            {

                //Give space .
                EditorGUILayout.Space();
                //Give a label
                GUILayout.Label("Google Test Add Type", EditorStyles.boldLabel);
                //TestAdType enum.
                googleAdMobManager.ShowTestAdd = (GoogleAdMobController.TestAdType)EditorGUILayout.EnumPopup(
                    "TestAdd Type",
                    googleAdMobManager.ShowTestAdd);
                //Give space .
                EditorGUILayout.Space();

                //Give a label
                GUILayout.Label("Google AdMobManager Target OS", EditorStyles.boldLabel);

                //Target Os enum.
                googleAdMobManager.TargetDeviceType = (GoogleAdMobController.DeviceType)EditorGUILayout.EnumPopup(
                    "GoogleAdMobController Target Device Type",
                    googleAdMobManager.TargetDeviceType);

                //Switch Target Device 
                switch (googleAdMobManager.TargetDeviceType)
                {
                    case GoogleAdMobController.DeviceType.Android:
                        if (googleAdMobManager.ShowTestAdd == GoogleAdMobController.TestAdType.UsingGoogleTestAppId)
                        {
                            EditorGUILayout.Space();
                            //Ask for Android App id.
                            GUILayout.Label("Android App Id", EditorStyles.boldLabel);
                            googleAdMobManager.AndroidAppId =
                                EditorGUILayout.TextField("AndroidAppId : ", googleAdMobManager.AndroidAppId);
                        }
                        else
                        {
                            EditorGUILayout.Space();
                            GUILayout.BeginVertical("Android Setup");
                            //Ask for Android App id.
                            GUILayout.Label("Android App Id", EditorStyles.boldLabel);
                            googleAdMobManager.AndroidAppId = EditorGUILayout.TextField("AndroidAppId : ", googleAdMobManager.AndroidAppId);
                            //Ask for Android Banner App id.
                            GUILayout.Label("Android Banner App Id", EditorStyles.boldLabel);
                            googleAdMobManager.AndroidBannerAppId =
                                EditorGUILayout.TextField("AndroidBannerAppId : ", googleAdMobManager.AndroidBannerAppId);
                            //Ask for Android Interstitial id.
                            GUILayout.Label("Android Interstitial App Id", EditorStyles.boldLabel);
                            googleAdMobManager.AndroidInterstitialAppId =
                                EditorGUILayout.TextField("AndroidInterstitialAppId : ", googleAdMobManager.AndroidInterstitialAppId);
                            //Ask for Android RewardVideo id.
                            GUILayout.Label("Android RewardVideo App Id", EditorStyles.boldLabel);
                            googleAdMobManager.AndroidRewardVideoAppId =
                                EditorGUILayout.TextField("AndroidInterstitialAppId : ", googleAdMobManager.AndroidRewardVideoAppId);
                            GUILayout.EndVertical();
                        }

                        break;
                    case GoogleAdMobController.DeviceType.Ios:
                        if (googleAdMobManager.ShowTestAdd == GoogleAdMobController.TestAdType.UsingGoogleTestAppId)
                        {
                            EditorGUILayout.Space();
                            //Ask for iOS App id.
                            GUILayout.Label("iOS App Id", EditorStyles.boldLabel);
                            googleAdMobManager.IosAppId =
                                EditorGUILayout.TextField("iOSAppId : ", googleAdMobManager.IosAppId);
                        }
                        else
                        {
                            EditorGUILayout.Space();
                            GUILayout.BeginVertical("iOS Setup");
                            //Ask for iOS App id.
                            GUILayout.Label("iOS App Id", EditorStyles.boldLabel);
                            googleAdMobManager.IosAppId = EditorGUILayout.TextField("iOSAppId : ", googleAdMobManager.IosAppId);
                            //Ask for iOS Banner App id.
                            GUILayout.Label("iOS Banner App Id", EditorStyles.boldLabel);
                            googleAdMobManager.AndroidBannerAppId =
                                EditorGUILayout.TextField("iOSBannerAppId : ", googleAdMobManager.IosBannerAppId);
                            //Ask for iOS Interstitial id.
                            GUILayout.Label("iOS Interstitial App Id", EditorStyles.boldLabel);
                            googleAdMobManager.AndroidInterstitialAppId =
                                EditorGUILayout.TextField("iOSInterstitialAppId : ", googleAdMobManager.IosInterstitialAppId);
                            //Ask for iOS RewardVideo id.
                            GUILayout.Label("iOS RewardVideo App Id", EditorStyles.boldLabel);
                            googleAdMobManager.AndroidRewardVideoAppId =
                                EditorGUILayout.TextField("iOSInterstitialAppId : ", googleAdMobManager.IosRewardVideoAppId);
                            GUILayout.EndVertical();
                        }

                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                //Check the type of show add.
                if (googleAdMobManager.ShowTestAdd != GoogleAdMobController.TestAdType.UsingTestDeviceIdentifier) return;
                //Give a label
                GUILayout.Label("Test Devices", EditorStyles.boldLabel);
                //Test Device List .
                var testDevices = googleAdMobManager.TestDevices;
                //Create a list 
                var newCount = Mathf.Max(0, EditorGUILayout.IntField("size", testDevices.Count));
                //Update the list .
                while (newCount < testDevices.Count)
                    testDevices.RemoveAt(testDevices.Count - 1);
                while (newCount > testDevices.Count)
                    testDevices.Add(null);
                //Add items in the Layout .
                for (var i = 0; i < testDevices.Count; i++)
                {
                    testDevices[i] = EditorGUILayout.TextField(testDevices[i]);
                }

                //Give space .
                EditorGUILayout.Space();

                
                //return.
                return;
            }
            //Give space .
            EditorGUILayout.Space();

            //Give a label
            GUILayout.Label("Google AdMobManager Target OS", EditorStyles.boldLabel);

            //Target Os enum.
            googleAdMobManager.TargetDeviceType = (GoogleAdMobController.DeviceType)EditorGUILayout.EnumPopup(
                "GoogleAdMobController Target Device Type",
                googleAdMobManager.TargetDeviceType);

            //Switch Target Device 
            switch (googleAdMobManager.TargetDeviceType)
            {
                //If Android.
                case GoogleAdMobController.DeviceType.Android:
                    GUILayout.BeginVertical("Android Setup");
                    EditorGUILayout.Space();
                    //Ask for Android App id.
                    GUILayout.Label("Android App Id", EditorStyles.boldLabel);
                    googleAdMobManager.AndroidAppId = EditorGUILayout.TextField("AndroidAppId : ", googleAdMobManager.AndroidAppId);
                    //Ask for Android Banner App id.
                    GUILayout.Label("Android Banner App Id", EditorStyles.boldLabel);
                    googleAdMobManager.AndroidBannerAppId = 
                        EditorGUILayout.TextField("AndroidBannerAppId : ", googleAdMobManager.AndroidBannerAppId);
                    //Ask for Android Interstitial id.
                    GUILayout.Label("Android Interstitial App Id", EditorStyles.boldLabel);
                    googleAdMobManager.AndroidInterstitialAppId = 
                        EditorGUILayout.TextField("AndroidInterstitialAppId : ", googleAdMobManager.AndroidInterstitialAppId);
                    //Ask for Android RewardVideo id.
                    GUILayout.Label("Android RewardVideo App Id", EditorStyles.boldLabel);
                    googleAdMobManager.AndroidRewardVideoAppId =
                        EditorGUILayout.TextField("AndroidInterstitialAppId : ", googleAdMobManager.AndroidRewardVideoAppId);
                    GUILayout.EndVertical();
                    break;
                //If iOS.
                case GoogleAdMobController.DeviceType.Ios:
                    GUILayout.BeginVertical("iOS Setup");
                    EditorGUILayout.Space();
                    //Ask for iOS App id.
                    GUILayout.Label("iOS App Id", EditorStyles.boldLabel);
                    googleAdMobManager.IosAppId = EditorGUILayout.TextField("iOSAppId : ", googleAdMobManager.IosAppId);
                    //Ask for iOS Banner App id.
                    GUILayout.Label("iOS Banner App Id", EditorStyles.boldLabel);
                    googleAdMobManager.AndroidBannerAppId =
                        EditorGUILayout.TextField("iOSBannerAppId : ", googleAdMobManager.IosBannerAppId);
                    //Ask for iOS Interstitial id.
                    GUILayout.Label("iOS Interstitial App Id", EditorStyles.boldLabel);
                    googleAdMobManager.AndroidInterstitialAppId =
                        EditorGUILayout.TextField("iOSInterstitialAppId : ", googleAdMobManager.IosInterstitialAppId);
                    //Ask for iOS RewardVideo id.
                    GUILayout.Label("iOS RewardVideo App Id", EditorStyles.boldLabel);
                    googleAdMobManager.AndroidRewardVideoAppId =
                        EditorGUILayout.TextField("iOSInterstitialAppId : ", googleAdMobManager.IosRewardVideoAppId);
                    GUILayout.EndVertical();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

        }

    }

}
